package dev.frjrosas.port;

import dev.frjrosas.infra.ConfigurationServiceImpl;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

class ConfigurationServiceTest {

  private ConfigurationService service;

  @BeforeEach
  public void setup() {
    service = mock(ConfigurationServiceImpl.class);
  }

  @Test
  @DisplayName("Read a route correctly")
  void getDroneRoute_fileIsCorrectlyRead() {
    String filename = "droneIn01.txt";
    when(service.getDroneRoutes(filename)).thenCallRealMethod();
    var routes = service.getDroneRoutes(filename);
    assertNotNull(routes);
    assertEquals(3, routes.size());
  }

  @Test
  @DisplayName("Load Drones Configuration with input")
  void getActiveDrones_firstConstructor() {
    var otherInstance = new ConfigurationServiceImpl(3, 3, 10);
    assertEquals(3, otherInstance.getMaxActiveDrones());
    assertEquals(3, otherInstance.getMaxDeliveriesPerDrone());
    assertEquals(10, otherInstance.getMaxSteps());
  }

  @Test
  @DisplayName("Load Drones Configuration from file")
  void getActiveDrones_secondConstructor_emptyFile() {
    var otherInstance = new ConfigurationServiceImpl("configuration_empty_file.in");
    assertEquals(20, otherInstance.getMaxActiveDrones());
  }

  @Test
  @DisplayName("Load Drones Configuration from file with valid value")
  void getActiveDrones_secondConstructor_validFile() {
    var otherInstance = new ConfigurationServiceImpl("configuration_valid_file.in");
    assertEquals(7, otherInstance.getMaxActiveDrones());
  }
}
