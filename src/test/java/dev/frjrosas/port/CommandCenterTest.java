package dev.frjrosas.port;

import dev.frjrosas.infra.ConfigurationServiceImpl;
import dev.frjrosas.infra.CorrientazoCommandCenter;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

class CommandCenterTest {

  private ConfigurationService configurationService;

  @BeforeEach
  public void setup() {
    configurationService = mock(ConfigurationServiceImpl.class);
  }

  @Test
  @DisplayName("Adding drone when drone quantity is not valid")
  void addDrone_droneQuantityIsAtOne_droneIsNotAdded() {
    when(configurationService.getMaxActiveDrones()).thenReturn(3);
    var commandCenter = new CorrientazoCommandCenter(configurationService);

    var actualDrones = commandCenter.getActiveDrones();
    commandCenter.addDrone();
    assertEquals(actualDrones, commandCenter.getActiveDrones());
  }

  @Test
  @DisplayName("Adding drone when drone quantity is valid")
  void addDrone_droneQuantityIsAt_droneIsAdded() {
    when(configurationService.getMaxActiveDrones()).thenReturn(3);
    var commandCenter = new CorrientazoCommandCenter(configurationService);

    commandCenter.reduceDrone();
    var actualDrones = commandCenter.getActiveDrones();

    commandCenter.addDrone();
    assertTrue(commandCenter.getActiveDrones() > actualDrones);
  }

  @Test
  @DisplayName("Reduce drones when drone quantity is valid")
  void reduceDrone_dronesQuantityIsValid_dronesAreReduced() {
    when(configurationService.getMaxActiveDrones()).thenReturn(3);
    var commandCenter = new CorrientazoCommandCenter(configurationService);

    var actualDrones = commandCenter.getActiveDrones();
    commandCenter.reduceDrone();
    assertNotEquals(actualDrones, commandCenter.getActiveDrones());
  }

  @Test
  @DisplayName("Reduce drone when drone quantity is minimum level")
  void reduceDrone_dronesQuantityIsOne_dronesAreNotReduced() {
    when(configurationService.getMaxActiveDrones()).thenReturn(1);
    var commandCenter = new CorrientazoCommandCenter(configurationService);

    var actualDrones = commandCenter.getActiveDrones();
    commandCenter.reduceDrone();
    assertEquals(actualDrones, commandCenter.getActiveDrones());
  }

  @Test
  @DisplayName("Reduce packages delivered by drones")
  void reducePackage_packagesAreValid_packagesAreReduced() {
    when(configurationService.getMaxDeliveriesPerDrone()).thenReturn(3);
    var commandCenter = new CorrientazoCommandCenter(configurationService);

    var actualPackages = commandCenter.getAllowedPackages();
    commandCenter.reducePackages();
    assertTrue(actualPackages > commandCenter.getAllowedPackages());
  }

  @Test
  @DisplayName("Reduce packages when deliverable packages at a minimum level")
  void reducePackage_packageIsOne_packagesAreNotReduced() {
    when(configurationService.getMaxDeliveriesPerDrone()).thenReturn(1);
    var commandCenter = new CorrientazoCommandCenter(configurationService);

    var actualPackages = commandCenter.getAllowedPackages();
    commandCenter.reducePackages();
    assertEquals(actualPackages, commandCenter.getAllowedPackages());
  }

  @Test
  @DisplayName("Increase deliverable packages per drone")
  void increasePackage(){
    when(configurationService.getMaxDeliveriesPerDrone()).thenReturn(3);
    var commandCenter = new CorrientazoCommandCenter(configurationService);

    var actualPackages = commandCenter.getAllowedPackages();
    commandCenter.increasePackages();
    assertTrue(actualPackages < commandCenter.getAllowedPackages());
  }
}
