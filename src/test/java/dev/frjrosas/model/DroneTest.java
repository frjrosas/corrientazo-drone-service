package dev.frjrosas.model;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

class DroneTest {

  @Test
  @DisplayName("Can Complete the route")
  void canCompleteIt_routeIsValid_trueIsReturned() {
    var route = new Route("AAAAIAA");
    var drone = new Drone("01");
    var result = drone.canCompleteIt(route, 10);
    String[] DIRECTIONS = {"N", "E", "S", "W"};
    var direction =
        DIRECTIONS[
            (int) Math.abs(Math.round(((Math.toDegrees(Math.atan2(4, -2)) % 360) / 90))) % 4];
    System.out.println(Math.toDegrees(Math.atan2(-3, 3)));
    System.out.println(direction);
    assertTrue(result);
  }

  @Test
  void canCompleteIt_routeIsNotValid_falseIsReturned() {
    var route = new Route("AAAAIAAAAAAAAAA");
    var drone = new Drone("01");
    var result = drone.canCompleteIt(route, 10);
    assertFalse(result);
  }

  @Test
  @DisplayName("")
  void canCompleteIt_routeIsNotValid_falseIsReturned2() {
    var route = new Route("AAAAIAA");
    var drone = new Drone("01");
    drone.canCompleteIt(route, 10);
    route = new Route("DDDAIAD");
    drone.canCompleteIt(route, 10);
    route = new Route("DDDAIAD");
    var result = drone.canCompleteIt(route, 10);
    assertTrue(result);
  }

  @Test
  @DisplayName("")
  void getDegrees() {
    var route = new Route("AAAAIAA");
    var drone = new Drone("01");
    var result = drone.canCompleteIt(route, 10);
    assertTrue(result);
  }

  @Test
  @DisplayName("")
  void deliver() {
    var routes =
        List.of(
            new Route("AAAAIAA"),
            new Route("DDDAIAD"),
            new Route("DDDA"),
            new Route("AADA"),
            new Route("IIADA"),
            new Route("ADIIADA"),
            new Route("IIADADDA"));
    var drone = new Drone("01");
    var list = drone.deliver(routes, 3, 10);
    assertEquals(7, list.size());
  }
}
