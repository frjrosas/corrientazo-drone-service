package dev.frjrosas.port;

public interface CommandCenter {

    void addDrone();

    void reduceDrone();

    int getActiveDrones();

    int getAllowedPackages();

    void reducePackages();

    void increasePackages();

    void processRequest();
}
