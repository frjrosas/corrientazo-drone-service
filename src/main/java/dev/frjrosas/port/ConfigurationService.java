package dev.frjrosas.port;

import dev.frjrosas.model.Route;

import java.util.List;

public interface ConfigurationService {
    List<Route> getDroneRoutes(String filename);

    void storeEvents(List<String> events, String filename);

    int getMaxActiveDrones();

    int getMaxDeliveriesPerDrone();

    int getMaxSteps();
}
