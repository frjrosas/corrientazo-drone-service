package dev.frjrosas.port;

import dev.frjrosas.model.Route;

import java.util.List;

public interface Movement {

    boolean canCompleteIt(Route route, int limit);

    List<Boolean> canCompleteItAll(List<Route> routeList, int limit);
}
