package dev.frjrosas.model;

public class Route {
  private final String input;

  public Route(String route) {
    this.input = route;
  }

  public String getRoute() {
    return this.input;
  }
}
