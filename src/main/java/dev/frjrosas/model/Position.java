package dev.frjrosas.model;

import dev.frjrosas.enumerate.DirectionEnum;
import dev.frjrosas.enumerate.MovementEnum;

public class Position {

  private static final String[] DIRECTIONS = {"N", "NE", "E", "SE", "S", "SW", "W", "NW"};
  private int abscissa;
  private int ordinate;
  private DirectionEnum direction;

  public Position() {
    this.abscissa = 0;
    this.ordinate = 0;
    this.direction = DirectionEnum.NORTH;
  }

  public Position(int abscissa, int ordinate, DirectionEnum direction) {
    this.abscissa = abscissa;
    this.ordinate = ordinate;
    this.direction = direction;
  }

  public Position(Position position){
    this(position.abscissa, position.ordinate, position.direction);
  }

  public void move() {
    switch (this.direction) {
      case NORTH:
        toNorth();
        break;
      case EAST:
        toEast();
        break;
      case SOUTH:
        toSouth();
        break;
      case WEST:
        toWest();
        break;
    }
  }

  private void toNorth() {
    this.ordinate += 1;
  }

  private void toSouth() {
    this.ordinate -= 1;
  }

  private void toEast() {
    this.abscissa += 1;
  }

  private void toWest() {
    this.abscissa -= 1;
  }

  public void reset() {
    this.abscissa = 0;
    this.ordinate = 0;
    this.direction = DirectionEnum.NORTH;
  }

  public int getAbscissa() {
    return this.abscissa;
  }

  public int getOrdinate() {
    return this.ordinate;
  }

  public DirectionEnum getDirection(){
    return this.getDirection();
  }

  public double getAngle() {
    return Math.atan2(-getOrdinate(), -getAbscissa());
  }

  public DirectionEnum calculateDirection() {
    var value = DIRECTIONS[(int) Math.abs(Math.round(((getAngle() % 360) / 45))) % 8];
    return DirectionEnum.get(value);
  }

  public void changeDirection(String step) {
    this.direction = DirectionEnum.getNextMovement(this.direction, MovementEnum.get(step));
  }

  @Override
  public String toString() {
    return "(" + abscissa + "," + +ordinate + ") dirección " + direction.getDisplay();
  }
}
