package dev.frjrosas.model;

import dev.frjrosas.enumerate.MovementEnum;
import dev.frjrosas.exception.BusinessException;
import dev.frjrosas.port.Movement;
import org.apache.commons.collections4.ListUtils;

import java.util.Arrays;
import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;

public class Drone implements Movement {
  private final String id;
  private Position position;
  private Position originalPosition;

  public Drone(String id) {
    this.id = id;
    position = new Position();
    originalPosition = new Position();
  }

  public List<String> deliver(List<Route> packages, int allowedDeliveries, int maxSteps) {
    this.position.reset();
    var deliveries = ListUtils.partition(packages, allowedDeliveries);
    return deliveries.stream()
        .map(batch -> deliver(batch, maxSteps))
        .flatMap(Collection::stream)
        .collect(Collectors.toList());
  }

  private List<String> deliver(List<Route> lunchesSteps, int maxSteps) {
    var resume =
        lunchesSteps.stream().map(steps -> deliver(steps, maxSteps)).collect(Collectors.toList());
    resume.add("== Regresando a origen ==");
    position.reset();
    return resume;
  }

  public String deliver(Route lunchSteps, int limit) {
    if (hasReachedLimit(true, limit) || hasReachedLimit(false, limit)){
      return "No se pueden hacer mas entregas";
    } else {
      move(lunchSteps);
      if (!hasReachedLimit(true, limit) && !hasReachedLimit(false, limit)) {
        originalPosition = new Position(position);
        return this.position.toString();
      } else {
        return "No se pueden hacer mas entregas";
      }
    }
  }

  @Override
  public boolean canCompleteIt(Route route, int limit) {
    move(route);
    var isReachable = !hasReachedLimit(true, limit) && !hasReachedLimit(false, limit);
    position = new Position(originalPosition);
    return isReachable;
  }

  private void move(Route route) {
    Arrays.stream(route.getRoute().split("")).forEach(this::processStep);
  }

  private void processStep(String step) {
    if (MovementEnum.isTurn(step)) {
      position.changeDirection(step);
    } else if (MovementEnum.isForward(step)) {
      position.move();
    } else {
      throw new BusinessException(2, "Command does not exist");
    }
  }

  private boolean hasReachedLimit(boolean isAbscissa, int limit) {
    var value = isAbscissa ? position.getAbscissa() : position.getOrdinate();
    return Math.abs(value) >= limit;
  }

  @Override
  public List<Boolean> canCompleteItAll(List<Route> routeList, int limit) {
    return routeList.stream()
        .map(route -> this.canCompleteIt(route, limit))
        .collect(Collectors.toList());
  }

  public String getId() {
    return this.id;
  }
}
