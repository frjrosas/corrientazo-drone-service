package dev.frjrosas.infra;

import dev.frjrosas.exception.TechnicalException;
import dev.frjrosas.model.Route;
import dev.frjrosas.port.ConfigurationService;
import org.apache.commons.lang3.math.NumberUtils;

import java.io.File;
import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Collections;
import java.util.List;
import java.util.Optional;
import java.util.logging.Level;
import java.util.logging.Logger;

import static java.util.stream.Collectors.toList;

public class ConfigurationServiceImpl implements ConfigurationService {

  private static final Logger LOGGER = Logger.getLogger("ConfigurationServiceImpl");

  private static final int DEFAULT_MAX_DRONES = 20;
  private static final int DEFAULT_MAX_DELIVERIES = 3;
  private static final int DEFAULT_MAX_STEPS = 10;

  private int maxDrones;
  private int maxDeliveriesPerDrone;
  private int maxSteps;

  public ConfigurationServiceImpl(int maxDrones, int maxDeliveriesPerDrone, int maxSteps) {
    this.maxDrones = maxDrones;
    this.maxDeliveriesPerDrone = maxDeliveriesPerDrone;
    this.maxSteps = maxSteps;
  }

  public ConfigurationServiceImpl(String filename) {
    handleInit(filename);
  }

  @Override
  public List<Route> getDroneRoutes(String filename) {
    var loader = getClass().getClassLoader();
    var resource = loader.getResource(filename);
    return loadRoutesFile(resource);
  }

  @Override
  public void storeEvents(List<String> events, String filename) {
    try {
      File file = new File(filename);
      file.createNewFile();
      Path path = Paths.get(filename);
      Files.write(path, events, StandardCharsets.UTF_8);
      events.add(0, "== Reporte de entregas ==");
      Files.write(path, events);
    } catch (IOException e) {
      e.printStackTrace();
      LOGGER.log(Level.FINEST, "There was an error writing the file", e);
      throw new TechnicalException(13, "The file could not be written");
    }
  }

  private void writeEvents(File file, List<String> events) {
    try {
      file.createNewFile();
      var path = file.toPath();
      Files.writeString(path, "== Reporte de entregas ==");
      Files.write(path, events);
    } catch (IOException e) {
      e.printStackTrace();
      LOGGER.log(Level.FINEST, "There was an error writing the file", e);
      throw new TechnicalException(13, "The file could not be written");
    }
  }

  @Override
  public int getMaxActiveDrones() {
    return this.maxDrones;
  }

  @Override
  public int getMaxDeliveriesPerDrone() {
    return this.maxDeliveriesPerDrone;
  }

  @Override
  public int getMaxSteps() {
    return this.maxSteps;
  }

  private List<Route> loadRoutesFile(URL resource) {
    return readLines(resource)
        .map(lines -> lines.parallelStream().map(Route::new).collect(toList()))
        .orElse(Collections.emptyList());
  }

  private void loadConfigurationFile(URL resource) {
    readLines(resource).ifPresentOrElse(this::processConfiguration, () -> this.maxDrones = 20);
  }

  private void processConfiguration(List<String> lines) {
    if (lines.isEmpty()) {
      this.maxDrones = DEFAULT_MAX_DRONES;
      this.maxDeliveriesPerDrone = DEFAULT_MAX_DELIVERIES;
      this.maxSteps = DEFAULT_MAX_STEPS;
    } else {
      this.maxDrones = getValue(lines, 0, DEFAULT_MAX_DRONES);
      this.maxDeliveriesPerDrone = getValue(lines, 1, DEFAULT_MAX_DELIVERIES);
      this.maxSteps = getValue(lines, 2, DEFAULT_MAX_STEPS);
    }
  }

  private int getValue(List<String> lines, int index, int defaultValue) {
    var value = lines.get(index);
    return NumberUtils.isDigits(value) ? Integer.parseInt(value) : defaultValue;
  }

  private Optional<List<String>> readLines(URL resource) {
    return Optional.ofNullable(resource)
        .map(this::handleURL)
        .map(File::new)
        .map(File::toPath)
        .map(this::getLines);
  }

  private URI handleURL(URL resource) {
    try {
      return resource.toURI();
    } catch (URISyntaxException e) {
      LOGGER.log(Level.FINEST, "The name of file has an invalid syntax", e);
      throw new TechnicalException(
          11, "There was an issue with the name of the file, please check it");
    }
  }

  private List<String> getLines(Path path) {
    try {
      return Files.readAllLines(path, StandardCharsets.UTF_8);
    } catch (IOException e) {
      LOGGER.log(Level.FINEST, "There was an error reading the file", e);
      throw new TechnicalException(12, "The file could not be read");
    }
  }

  private void handleInit(String filename) {
    var loader = getClass().getClassLoader();
    var resource = loader.getResource(filename);
    loadConfigurationFile(resource);
  }
}
