package dev.frjrosas.infra;

import dev.frjrosas.model.Drone;
import dev.frjrosas.port.CommandCenter;
import dev.frjrosas.port.ConfigurationService;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.IntStream;

public class CorrientazoCommandCenter implements CommandCenter {

  private final ConfigurationService configurationService;
  private final List<Drone> drones;
  private int maxDeliveries;
  private int maxSteps;

  public CorrientazoCommandCenter(ConfigurationService configurationService) {
    this.configurationService = configurationService;
    this.drones = new ArrayList<>();

    this.maxDeliveries = configurationService.getMaxDeliveriesPerDrone();
    this.maxSteps = configurationService.getMaxSteps();
    IntStream.rangeClosed(1, configurationService.getMaxActiveDrones())
        .mapToObj(this::buildDroneId)
        .map(Drone::new)
        .forEach(drones::add);
  }

  @Override
  public void addDrone() {
    if (drones.size() < configurationService.getMaxActiveDrones()) {
      var qty = drones.size();
      drones.add(new Drone(buildDroneId(qty + 1)));
    }
  }

  private String buildDroneId(int i) {
    return i > 9 ? String.valueOf(i) : "0" + i;
  }

  @Override
  public void reduceDrone() {
    if (drones.size() > 1) {
      drones.remove(drones.size() - 1);
    }
  }

  @Override
  public int getActiveDrones() {
    return drones.size();
  }

  @Override
  public int getAllowedPackages() {
    return this.maxDeliveries;
  }

  @Override
  public void reducePackages() {
    if (maxDeliveries > 1) {
      maxDeliveries--;
    }
  }

  @Override
  public void increasePackages() {
    maxDeliveries++;
  }

  @Override
  public void processRequest() {
    drones.parallelStream().forEach(this::command);
  }

  private void command(Drone drone) {
    var inFilename = "in" + drone.getId() + ".txt";
    var outFilename = "out" + drone.getId() + ".txt";
    var routes = configurationService.getDroneRoutes(inFilename);
    var notifications = drone.deliver(routes, maxDeliveries, maxSteps);
    configurationService.storeEvents(notifications, outFilename);
  }
}
