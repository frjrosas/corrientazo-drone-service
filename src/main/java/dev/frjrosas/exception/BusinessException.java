package dev.frjrosas.exception;

public class BusinessException extends RuntimeException {
    private final int code;
    private final String msg;

    public BusinessException(int code, String msg){
        this.code = code;
        this.msg = msg;
    }
}
