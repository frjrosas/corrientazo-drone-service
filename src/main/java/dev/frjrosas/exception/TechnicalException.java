package dev.frjrosas.exception;

public class TechnicalException extends RuntimeException {
    private final int code;

    private final String msg;

    public TechnicalException(int code, String msg){
        this.code = code;
        this.msg = msg;
    }
}
