package dev.frjrosas;

import dev.frjrosas.infra.ConfigurationServiceImpl;
import dev.frjrosas.infra.CorrientazoCommandCenter;

public class Main {
  public static void main(String args[]) {
    var configurationService = new ConfigurationServiceImpl("initial_config.in");
    var commandCenter = new CorrientazoCommandCenter(configurationService);
    commandCenter.processRequest();
  }
}
