package dev.frjrosas.enumerate;

public enum MovementEnum {
  RIGHT("D"),
  LEFT("I"),
  FORWARD("A");

  private final String direction;

  MovementEnum(String direction) {
    this.direction = direction;
  }

  public static boolean isTurn(String input) {
    return RIGHT.direction.equals(input) || LEFT.direction.equals(input);
  }

  public static boolean isForward(String input) {
    return FORWARD.direction.equals(input);
  }

  public static MovementEnum get(String movement) {
    switch (movement) {
      case "D":
        return RIGHT;
      case "I":
        return LEFT;
      default:
        return FORWARD;
    }
  }
}
