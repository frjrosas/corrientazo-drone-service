package dev.frjrosas.enumerate;

public enum DirectionEnum {
  NORTH("N", "Norte", "W", "E"),
  EAST("E", "Oriente", "N", "S"),
  SOUTH("S", "Sur", "E", "W"),
  WEST("W", "Occidente", "S", "N");

  private final String direction;
  private final String display;
  private final String left;
  private final String right;

  DirectionEnum(String direction, String display, String left, String right) {
    this.direction = direction;
    this.display = display;
    this.right = right;
    this.left = left;
  }

  public static DirectionEnum getNextMovement(DirectionEnum current, MovementEnum movement) {
    switch (movement) {
      case LEFT:
        return get(current.left);
      case RIGHT:
        return get(current.right);
      default:
        return current;
    }
  }

  public static DirectionEnum get(String direction) {
    switch (direction) {
      case "N":
        return NORTH;
      case "E":
        return EAST;
      case "W":
        return WEST;
      case "S":
        return SOUTH;
      default:
        return null;
    }
  }

  public String getValue() {
    return this.direction;
  }

  public String getDisplay(){
    return this.display;
  }
}
